var resources = [
	{
		title: "QArt Codes",
		desc: "How to use QR code generation algorithm to draw images in fully valid qr codes",
		tags: ["QR", "Art", "Front-end"],
		link: "https://research.swtch.com/qart",
		imageLink:
			"https://research.swtch.com/qart1.png",
		date: "Sep 06 2023",
	},
	{
		title: "How Did They Do That? The Twitter “Like” Animation.",
		desc: "CSS animation optimisation tutorial using a sprite sheet.",
		tags: ["Design", "CSS", "Front-end"],
		link: "https://medium.com/@chrismabry/how-did-they-do-that-the-twitter-like-animation-2a473b658e43",
		imageLink:
			"https://miro.medium.com/v2/resize:fit:720/1*HJHfcRwn33XU4omMogi6PQ.gif",
		date: "Jul 25 2023",
	},
	{
		title: "The CSS Cascade",
		desc: "Or, How browsers resolve competing CSS styles. Intersting css explaination with a beautiful layout.",
		tags: ["CSS", "Front-end"],
		link: "https://2019.wattenberger.com/blog/css-cascade",
		imageLink:
			"https://2019.wattenberger.com/static/media/css-cascade.cd1e324d.png",
		date: "Jun 1 2023",
	},
	{
		title: "Designing for Speed | Google Senior UX Designer | Mustafa Kurtuldu",
		desc: "UX conference speech on how to to increase perceived performance in websites and apps.",
		tags: ["Design", "UX", "Front-end"],
		link: "https://www.youtube.com/watch?v=Drf5ZKd4aVY",
		imageLink:
			"https://assets.awwwards.com/awards/images/2020/04/mustafa-talk-thumbnail.jpg",
		date: "Jul 12 2021",
	},
	{
		title: "Practice CSS Like an Artist",
		desc: "CSS lessons you can learn from painting tips.",
		tags: ["Web", "Design", "CSS", "Painting"],
		link: "https://mastery.games/post/practice-css",
		imageLink: "https://mastery.games/img/practice-css.jpg",
		date: "Jan 26 2021",
	},
	{
		title: "UI Design Trend of 2021",
		desc: "Glassmorphism is possibly the new UI Design Trend of 2021.",
		tags: ["Web", "Design", "Trend"],
		link: "https://dev.to/harshhhdev/ui-design-trend-of-2021-4fb7",
		imageLink:
			"https://res.cloudinary.com/practicaldev/image/fetch/s--PXDu1704--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://cdn.dribbble.com/users/5008109/screenshots/14702832/media/7ab3b7e6f73b0493ec7693bdc1d3ad20.png",
		date: "Dec 08 2020",
	},
	{
		title: "Creativity x Machine Learning",
		desc: "A collection of Machine Learning experiments.",
		tags: ["Generative Art", "Code", "Gallery"],
		link: "https://mlart.co/",
		imageLink: "https://i.imgur.com/y8mIZyK.gif",
		date: "Oct 27 2020",
	},
	{
		title: "SVG Favicon Maker",
		desc: "Simple online tool to create a favicon from a color, font and other parameters. Really useful if you want to easily get rid of the default favicon for a small website.",
		tags: ["Web", "Design", "Tool"],
		link: "https://formito.com/tools/favicon",
		imageLink:
			"https://ps.w.org/formito/assets/icon-128x128.png?rev=2399621",
		date: "Oct 23 2020",
	},
	{
		title: "Melodysheep, Remixing the universe. One video at a time.",
		desc: "One of my favorite videomaker, making 3D animated scientific videos about the wonders of the universe.",
		tags: ["3D Animation", "Space", "Documentary"],
		link: "https://www.melodysheep.com/",
		imageLink:
			"https://images.squarespace-cdn.com/content/v1/595f00fc36e5d3078c629dd7/1574275404133-BLXB8CX6CLIZ0ZIGGLIH/ke17ZwdGBToddI8pDm48kPTrHXgsMrSIMwe6YW3w1AZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0k5fwC0WRNFJBIXiBeNI5fKTrY37saURwPBw8fO2esROAxn-RKSrlQamlL27g22X2A/Thumb1.jpg?format=1000w",
		date: "Oct 10 2020",
	},
	{
		title: "OpenAI's GPT-3 may be the biggest thing since bitcoin",
		desc: "Very interesting article describing GPT-3, OpenAI's new language prediction model. ",
		tags: ["Article", "AI", "Language Prediction"],
		link: "https://maraoz.com/2020/07/18/openai-gpt3/",
		imageLink: "https://maraoz.com/img/openai-gpt3/cover.jpg",
		date: "Oct 8 2020",
	},
	{
		title: "Design Gems",
		desc: "This website gathers a lot of tools and ideas that might come in handy if you're a front-end developer.",
		tags: ["Resource", "Front-end"],
		link: "https://www.designgems.co/",
		imageLink: "https://www.designgems.co/assets/favicon.png",
		date: "Oct 8 2020",
	},
	{
		title: "Colors And Fonts",
		desc: "Tool to dig through a large number of beautiful color palettes, gradients and fonts.",
		tags: ["Resource", "Front-end"],
		link: "https://www.colorsandfonts.com/",
		imageLink:
			"https://d33wubrfki0l68.cloudfront.net/5080ea97af3db19857c074318eedb5a21e0f4a42/ab14b/images/thumbnails/palette.svg",
		date: "Oct 8 2020",
	},
];
var todos = [
	{
		title: "Boids simulation",
		desc: "To get better at Unreal Engine, i would need a more complex project. What better choice than to reproduce the bird flocking behavior model known as BOIDS",
		tags: ["Unreal Engine", "Unity", "Simulation"],
		relatedLinks: [
			{
				title: "Article describing the BOIDS model",
				link: "http://www.red3d.com/cwr/boids/",
			},
			{
				title: "BOIDS Pseudo code",
				link: "http://www.kfish.org/boids/pseudocode.html",
			},
		],
	},
	{
		title: "Boids obstacle course",
		desc: "If i get to complete the boids simulation, i would really like to play around with it a bit more. The goal of this game would be to go through an obstacle course by controlling one of the boids and having the others follow you. I would apply a greater weight to the player's direction vector to do so.",
		tags: ["Unreal Engine", "Unity", "Game dev"],
		relatedLinks: [
			{
				title: "Wii sports game that gave me the idea",
				link: "https://www.ign.com/wikis/wii-sports-resort/Island_Flyover",
			},
		],
	},
	{
		title: "Workout generator",
		desc: "Mobile app or website to randomly generate workouts based on set preferences and previously achieved goals. It would also include timer features and ",
		tags: ["Android", "Front-end", "Web"],
		relatedLinks: [
			{
				title: "Darebee, free access to fitness resourceswebsite ",
				link: "https://darebee.com",
			},
		],
	},
	{
		title: "RPG character sheet creator",
		desc: "As a game master during tabletop RPGs. A character sheet creator tool would really help me. Since i often use custom game systems. To create new character sheets for my players, i often resort to Adobe Illustrator. The goal of the website would be to create and update character sheet models based on a simple list of the needed characteristics.",
		tags: ["Front-end", "Web"],
		relatedLinks: [
			{
				title: "Cyberpunk red generator that gave me the idea",
				link: "https://cyberpunk-char-gen.neocities.org/#/",
			},
		],
	},
];
export { resources, todos };
